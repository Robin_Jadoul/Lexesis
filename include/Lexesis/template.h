/*
Lexesis - A language agnostic lexical analyser generator
Copyright © 2016-2017 Thomas Avé, Robin Jadoul

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef LEXESIS_TEMPLATE_H
#define LEXESIS_TEMPLATE_H

#include <map>
#include <string>
#include <vector>
#include <mstch/mstch.hpp>

namespace templ {
    /**
     * A changeable information structure for templates
     */
    using TemplateContext = mstch::node;

    /**
     * Make a TemplateContext string
     */
    TemplateContext make_string(std::string);

    /**
     * Make a TemplateContext map/dictionary
     */
    TemplateContext make_map(std::map<const std::string, TemplateContext>);

    /**
     * Make a TemplateContext array/vector
     */
    TemplateContext make_array(std::vector<TemplateContext>);

    /**
     * A generic wrapper around whichever templating system gets used
     */
    class Template {
        public:
            /**
             * Construct a Template from given filename
             *
             * @param filename The name of the file which contains the template rules
             */
            Template(std::string filename);

            /**
             * Destructor
             */
            ~Template();

            /**
             * Render this template to `out` using the information in `context`
             *
             * @param out The ostream to render to
             * @param context The information to provide the template rules while rendering
             */
            void render(std::ostream& out, TemplateContext& context);
        private:
            std::string m_filename;
    };

} //namespace templ

#endif //LEXESIS_TEMPLATE_H
