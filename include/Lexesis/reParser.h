#pragma once
#ifndef LEXESIS_RE_PARSER_H
#define LEXESIS_RE_PARSER_H

#include "Lexesis/RegexLexer.h"
#include "Lexesis/RegexParser.h"
#include "Lexesis/re.h"
#include <deque>
#include <memory>
#include <stdexcept>

namespace lxs {

class ReParser : public RegexParser<std::shared_ptr<RE>> {
    public:
        ReParser(RegexLexer lex);
        
    private:
        RegexParser::Token lex() override;

        std::shared_ptr<RE> reduce_char(std::deque<RegexParser::Token> subparts) override;
        std::shared_ptr<RE> reduce_concat(std::deque<RegexParser::Token> subparts) override;
        std::shared_ptr<RE> reduce_optional(std::deque<RegexParser::Token> subparts) override;
        std::shared_ptr<RE> reduce_or(std::deque<RegexParser::Token> subparts) override;
        std::shared_ptr<RE> reduce_paren(std::deque<RegexParser::Token> subparts) override;
        std::shared_ptr<RE> reduce_plus(std::deque<RegexParser::Token> subparts) override;
        std::shared_ptr<RE> reduce_star(std::deque<RegexParser::Token> subparts) override;

        RegexLexer m_lex;
};

	/**
     * Parse the given regular expression and return the associated Regex
     *
     * @param input The regular expression to parse
     * @returns An abstraction representation of `input`
     *
     * @throws SyntaxError if the regex is invalid, the `what()` method contains some information on the problem.
     */
    std::shared_ptr<RE> parseRE(const std::string& input);  

	/**
     * An exception to represent a syntax error in a regular expression
     */
    class SyntaxError : public std::runtime_error
    {
        public:
            SyntaxError(RegexLexer& lex, const std::string w) : std::runtime_error((std::to_string(lex.getByteOffset()) + ": " + w)) {}
    };  
}


#endif
