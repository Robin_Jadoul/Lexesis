/*
Lexesis - A language agnostic lexical analyser generator
Copyright © 2016-2017 Thomas Avé, Robin Jadoul

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef INPUT_PARSER_H 
#define INPUT_PARSER_H

#include <istream>
#include <vector>
#include <set>
#include <string>

namespace lxs {
namespace input {

    /**
     * Used for parsing token rules
     */
    class InputParser {
        public:
            /**
             * parse the tokens rules read from `is`
             *
             * std::vector<std::pair<int, std::pair<std::string,std::string>>>
             *                     <line number, <tokenname, regex> >
             */
            static std::vector<std::pair<int, std::pair<std::string,std::string>>> parseInput(std::istream& is);
            
            /**
             * Get a list of the tokens specified in the istream 
             *
             * @param is The istream to get the tokens from
             *
             * @return the list of tokens
             */
            static std::set<std::string> getTokens(std::istream& is);
    };
    
    /**
     * Used to throw errors when the inputfile was not valid
     */

    class InputParserException: public std::exception {
    public:
        InputParserException(std::string what);
        virtual const char* what() const throw();

    private:
        std::string m_what;
    };
}
}
#endif // INPUT_PARSER_H
