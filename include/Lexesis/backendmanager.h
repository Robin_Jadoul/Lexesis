/*
Lexesis - A language agnostic lexical analyser generator
Copyright © 2016-2017 Thomas Avé, Robin Jadoul

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef LEXESIS_BACKENDMANAGER_H
#define LEXESIS_BACKENDMANAGER_H

#include <memory>
#include <string>
#include <vector>

#include "Lexesis/backend.h"

namespace lxs {
    /**
     * A manager for backends
     * Aggregates and allow to search for backends that can process a specific language
     */
    class BackendManager {
        public:
            /**
             * Add a backend to the list of registered backends
             *
             * @param backend The backend to register
             */
            void registerBackend(std::unique_ptr<Backend> backend);

            /**
             * Get a backend that can process the given language
             * The manager retains ownership of the returned pointer
             *
             * @param lang The language the backend should be able to process
             * @returns A pointer to a Backend if it can find one, nullptr otherwise
             */
            Backend* findBackendForLang(std::string lang);

        private:
            std::vector<std::unique_ptr<Backend> > m_backends; ///< The list of registered backends
    };
}

#endif //LEXESIS_BACKENDMANAGER_H
