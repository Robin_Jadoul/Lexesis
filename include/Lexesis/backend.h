/*
Lexesis - A language agnostic lexical analyser generator
Copyright © 2016-2017 Thomas Avé, Robin Jadoul

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef LEXESIS_BACKEND_H
#define LEXESIS_BACKEND_H

#include "Lexesis/automata.h"
#include "Lexesis/template.h"

#include "mstch/mstch.hpp"

#include <functional>
#include <memory>
#include <string>

namespace lxs {
    /**
     * A general interface for a Lexesis backend
     */
    class Backend {
        public:
            /**
             * Constructor
             */
            Backend();

            /**
             * Destructor
             */
            virtual ~Backend();

            /**
             * Report a name for the backend
             * used in resolving template paths
             *
             * @return std::string A name for the backend
             */
            virtual std::string getName() = 0;

            /**
             * Can this backend process the language with given description?
             *
             * @param lang A description for a language (eg. "c++", "cxx", "cpp")
             * @return Can this backend process it
             */
            virtual bool canProcessLang(std::string lang);

            /**
             * The function that gets called to generate the actual lexer
             *
             * @param getOstreamForFileName A function that takes a filename and returns a std::ostream that the backend can write to for that filename
             * @param lexerName The name that should be given to the lexer
             * @param dfa The automaton that should be used in generating the lexer
             */
            virtual void generateLexer(std::function<std::unique_ptr<std::ostream>(std::string)> getOstreamForFileName, std::string lexerName, const DFA& dfa) = 0;

        protected:
            /**
             * Render a template (with given (file)name) to the given ostream with the information provided
             *
             * @param out The ostream to write the rendered template to
             * @param templateName An identifier for the template, is combined with `getName()` to construct the actual path
             * @param context The information that should be provided to the template when rendering
             */
            void doTemplate(std::ostream& out, std::string templateName, templ::TemplateContext context);

        private:
            /**
             * Find the template with given name
             *
             * @param templateName the template name, gets combined with `getName()`
             */
            std::string findTemplate(std::string templateName);
    };
}

#endif //LEXESIS_BACKEND_H

