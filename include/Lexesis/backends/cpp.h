/*
Lexesis - A language agnostic lexical analyser generator
Copyright © 2016-2017 Thomas Avé, Robin Jadoul

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#ifndef LEXESIS_BACKENDS_CPP_H
#define LEXESIS_BACKENDS_CPP_H

#include "Lexesis/backend.h"

namespace lxs
{
namespace backends
{
    /**
     * A backend that emits c++ code
     */
    class CppBackend : public Backend {
        public:
            CppBackend();
            virtual ~CppBackend();

            virtual std::string getName();
            virtual bool canProcessLang(std::string lang);

            virtual void generateLexer(std::function<std::unique_ptr<std::ostream>(std::string)> getOstreamForFileName, std::string lexerName, const DFA& dfa);

        private:
            /**
             * Build a TemplateContext that represents the transition table
             *
             * @param transition_idx \see buildTransitionIndices
             */
            templ::TemplateContext buildTable(const DFA& dfa, const std::vector<unsigned char>& transition_idx, int num_transitions_per_state) const;

            /**
             * Build a TemplateContext that represents the list of associated tokens with each state
             */
            templ::TemplateContext buildTokenList(const DFA& dfa) const;

            /**
             * For compression of the table, build a list that maps each char to an index
             * This way, whenever multiple chars always represent the same transition, the can get the same index, and the table is smaller
             *
             * @return a pair with the list and the number of distinct indices
             */
            std::pair<std::vector<unsigned char>, int> buildTransitionIndices(const DFA& dfa) const;

            /**
             * Transform the given indices (\see buildTransitionIndices) to a usable TemplateContext
             */
            templ::TemplateContext transformTransitionIndices(const std::vector<unsigned char>& transition_indices) const;
    };

}
}

#endif //LEXESIS_BACKENDS_CPP_H
