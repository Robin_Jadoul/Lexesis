/*
 * This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */
#pragma once
#ifndef LEXER_{{name}}_H
#define LEXER_{{name}}_H

#include <exception>
#include <istream>
#include <string>

class {{name}} {
    public:
        class NoMoreTokens : public std::exception {};
        class NoMatch : public std::exception {};

        enum TokenType {
            nonmatching,
            {{#token_types}}
            {{type}},
            {{/token_types}}
        };

        struct Token {
            TokenType type;
            std::string content;
        };
        
        {{name}}(std::istream& in);
        ~{{name}}();

        /**
         * Get the next token
         *
         * @throws NoMoreTokens if no more tokens are available
         * @throws NoMatch if no match was found
         */
        Token nextToken();

        /**
         * Skip the following `n` bytes.
         *
         * @param n The number of bytes to skip
         */
        void skip(std::size_t n);

        /**
         * Peek at the current head of the input stream, useful in error reporting when a character mismatches for example
         *
         * @throws NoMoreTokens if the input stream is at an end
         */
        char peek();

        /**
         * Get the current byte offset
         */
        std::size_t getByteOffset();

    private:
        std::size_t m_offset;
        std::istream& m_input;
};

#endif //LEXER_{{name}}_H
