/*
Lexesis - A language agnostic lexical analyser generator
Copyright © 2016-2017 Thomas Avé, Robin Jadoul

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "Lexesis/inputparser.h"

#include <istream>
#include <string>
#include <iostream>
#include <memory>
#include <exception>

namespace lxs {
namespace input {

    InputParserException::InputParserException(std::string what): m_what(what) {} 
    const char* InputParserException::what() const throw() {
            return m_what.c_str();
        }
    
    std::set<std::string> InputParser::getTokens(std::istream& is) {
        auto lines = parseInput(is);
        std::set<std::string> tokens;
        for(auto line: lines) {
            if(line.second.first != "ignore")
                tokens.insert(line.second.first);
        }
        return tokens;
    }

    std::vector<std::pair<int, std::pair<std::string,std::string>>> InputParser::parseInput(std::istream &is) {
        std::string line;
        std::vector<std::pair<int, std::pair<std::string,std::string> > > result;
        unsigned int i=0;
        while(std::getline(is,line)) {
            i++;
            size_t start = line.find_first_not_of(" \t\v\f\r");
            if(line.length() == 0 || (start != std::string::npos && line.length() > start && line[start] == '#')) continue;
            std::size_t loc = line.find_first_of('=');
            if(loc == std::string::npos) throw InputParserException("Invalid syntax on line " + std::to_string(i) + ": no '=' found!");
            if(start == loc) throw InputParserException("Invalid syntax on line " + std::to_string(i) + ": no valid tokenname specified!");
            std::string tokenname = line.substr(start, loc);
            if(tokenname.length() == 0) throw InputParserException("Invalid syntax on line " + std::to_string(i) + ": no valid tokenname specified!");
            std::size_t end = tokenname.find_last_not_of(" \t\v\f\r");
            tokenname = tokenname.substr(0,end + 1);
            std::string regex = line.substr(loc+1);
            if(regex.length() == 0) throw InputParserException("Invalid syntax on line " + std::to_string(i) + ": no valid regex specified!");
            start = regex.find_first_not_of(" \t\v\f\r");
            if(start == std::string::npos) throw InputParserException("Invalid syntax on line " + std::to_string(i) + ": no valid regex specified!");
            regex = regex.substr(start);
            result.push_back(std::make_pair(i,std::make_pair(tokenname,regex)));
        }
        if(result.size() == 0) throw InputParserException("No valid rules found in the input file!");
        return result;
    }

}
}
