#include <algorithm>
#include <cassert>
#include <codecvt>
#include <deque>
#include <locale>
#include <sstream>
#include "Lexesis/reParser.h"
#include <iostream>


namespace lxs {

    ReParser::ReParser(RegexLexer lex) : RegexParser<std::shared_ptr<RE>>(), m_lex(lex)
    {}

    namespace {
        std::shared_ptr<RE> dotChar() {
            std::vector<char> any;
            for (int i = 0; i < 256; i++)
                if ((char) i != '\n') //Dot matches anything except newlines
                    any.push_back((char) i);
            return std::make_shared<MultiRE>(any);
        }

        /**
         * Parse a character class
         */
        std::shared_ptr<RE> parseCharacterClass(const std::string& input) {
            std::set<char> used_chars;

            bool invert = false;
            std::size_t start = 1;
            std::size_t end = input.size() - 1;

            if (input[1] == '^') {
                invert = true;
                start = 2;
            }

            if (input[start] == ']') {
                used_chars.insert(']');
                ++start;
            }

            if (input[start] == '-') {
                used_chars.insert('-');
                ++start;
            }

            if (input[end - 1] == '-') {
                used_chars.insert('-');
                --end;
            }

            int last_char = -1;
            for (std::size_t idx = start; idx < end; idx++)
            {
                if (input[idx] == '-')
                {
                    idx++;
                    for (int i = last_char; i <= input[idx]; i++) {
                        used_chars.insert((char) i);
                    }
                    last_char = -1;
                }
                else
                {
                    if (idx == end - 1 || (idx < end - 1 && input[idx + 1] != '-'))
                        used_chars.insert(input[idx]);
                    else
                        last_char = input[idx];
                }
            }

            std::vector<char> chars;
            for (int i = 0; i < 256; i++) {
                if (invert ^ (used_chars.count((char) i) > 0))
                    chars.push_back((char) i);
            }

            return std::make_shared<MultiRE>(chars);
        }
    }

    RegexParser<std::shared_ptr<RE>>::Token ReParser::lex() {
        try {
            RegexLexer::Token orig = m_lex.nextToken();
            switch(orig.type) {
                case RegexLexer::TAB:
                    return Token(RegexParser_Symbol::T_TAB, std::make_shared<SingleRE>('\t'));
                case RegexLexer::NEWLINE:
                    return Token(RegexParser_Symbol::T_NEWLINE, std::make_shared<SingleRE>('\n'));
                case RegexLexer::CARRIAGE_RETURN:
                    return Token(RegexParser_Symbol::T_CARRIAGE_RETURN, std::make_shared<SingleRE>('\r'));
                case RegexLexer::BACKSPACE:
                    return Token(RegexParser_Symbol::T_BACKSPACE, std::make_shared<SingleRE>('\b'));
                case RegexLexer::SPACE:
                    return Token(RegexParser_Symbol::T_SPACE, std::make_shared<SingleRE>(' '));
                case RegexLexer::BELL:
                    return Token(RegexParser_Symbol::T_BELL, std::make_shared<SingleRE>('\a'));
                case RegexLexer::FORMFEED:
                    return Token(RegexParser_Symbol::T_FORMFEED, std::make_shared<SingleRE>('\f'));
                case RegexLexer::VTAB:
                    return Token(RegexParser_Symbol::T_VTAB, std::make_shared<SingleRE>('\v'));
                case RegexLexer::BACKSLASH:
                    return Token(RegexParser_Symbol::T_BACKSLASH, std::make_shared<SingleRE>(orig.content[1]));
                case RegexLexer::ESCAPED_STAR:
                    return Token(RegexParser_Symbol::T_ESCAPED_STAR, std::make_shared<SingleRE>(orig.content[1]));
                case RegexLexer::ESCAPED_PLUS:
                    return Token(RegexParser_Symbol::T_ESCAPED_PLUS, std::make_shared<SingleRE>(orig.content[1]));
                case RegexLexer::ESCAPED_PIPE:
                    return Token(RegexParser_Symbol::T_ESCAPED_PIPE, std::make_shared<SingleRE>(orig.content[1]));
                case RegexLexer::ESCAPED_LPAREN:
                    return Token(RegexParser_Symbol::T_ESCAPED_LPAREN, std::make_shared<SingleRE>(orig.content[1]));
                case RegexLexer::ESCAPED_RPAREN:
                    return Token(RegexParser_Symbol::T_ESCAPED_RPAREN, std::make_shared<SingleRE>(orig.content[1]));
                case RegexLexer::ESCAPED_LBRACKET:
                    return Token(RegexParser_Symbol::T_ESCAPED_LBRACKET, std::make_shared<SingleRE>(orig.content[1]));
                case RegexLexer::ESCAPED_RBRACKET:
                    return Token(RegexParser_Symbol::T_ESCAPED_RBRACKET, std::make_shared<SingleRE>(orig.content[1]));
                case RegexLexer::ESCAPED_QUESTIONMARK:
                    return Token(RegexParser_Symbol::T_ESCAPED_QUESTIONMARK, std::make_shared<SingleRE>(orig.content[1]));
                case RegexLexer::ESCAPED_DOT:
                    return Token(RegexParser_Symbol::T_ESCAPED_DOT, std::make_shared<SingleRE>(orig.content[1]));
                case RegexLexer::DOT:
                    return Token(RegexParser_Symbol::T_DOT, dotChar());
                case RegexLexer::STAR:
                    return Token(RegexParser_Symbol::T_STAR, std::shared_ptr<RE>(new EmptyRE()));
                case RegexLexer::PLUS:
                    return Token(RegexParser_Symbol::T_PLUS, std::shared_ptr<RE>(new EmptyRE()));
                case RegexLexer::QUESTIONMARK:
                    return Token(RegexParser_Symbol::T_QUESTIONMARK, std::shared_ptr<RE>(new EmptyRE()));
                case RegexLexer::PIPE:
                    return Token(RegexParser_Symbol::T_PIPE, std::shared_ptr<RE>(new EmptyRE()));
                case RegexLexer::LPAREN:
                    return Token(RegexParser_Symbol::T_LPAREN, std::shared_ptr<RE>(new EmptyRE()));
                case RegexLexer::RPAREN:
                    return Token(RegexParser_Symbol::T_RPAREN, std::shared_ptr<RE>(new EmptyRE()));
                case RegexLexer::CHAR:
                    return Token(RegexParser_Symbol::T_CHAR, std::make_shared<SingleRE>(orig.content[0]));
                case RegexLexer::CHAR_CLASS:
                    return Token(RegexParser_Symbol::T_DOT, parseCharacterClass(orig.content));
                case RegexLexer::ERROR: case RegexLexer::ignore: case RegexLexer::nonmatching:
                    throw SyntaxError(m_lex,"Received an error from the lexer while parsing token: " + orig.content);
                default:
                    return Token(RegexParser_Symbol::T_EOF, std::shared_ptr<RE>(new EmptyRE()));

            }
        } catch (RegexLexer::NoMoreTokens) {
            return Token(RegexParser_Symbol::T_EOF, std::shared_ptr<RE>(new EmptyRE()));
             
        }
    }
    
    std::shared_ptr<RE> ReParser::reduce_char(std::deque<RegexParser::Token> subparts) {
        return std::move(subparts[0].value);
    }
    std::shared_ptr<RE> ReParser::reduce_concat(std::deque<RegexParser::Token> subparts) {
        return std::make_shared<ConcatRE>(subparts[0].value, subparts[1].value);
    }
    std::shared_ptr<RE> ReParser::reduce_optional(std::deque<RegexParser::Token> subparts) {
        return std::make_shared<PlusRE>(subparts[0].value, std::make_shared<EpsilonRE>());
    }
    std::shared_ptr<RE> ReParser::reduce_or(std::deque<RegexParser::Token> subparts) {
        return std::make_shared<PlusRE>(subparts[0].value, subparts[2].value); 
    }
    std::shared_ptr<RE> ReParser::reduce_paren(std::deque<RegexParser::Token> subparts) {
        return std::move(subparts[1].value);
    }
    std::shared_ptr<RE> ReParser::reduce_plus(std::deque<RegexParser::Token> subparts) {
        return std::make_shared<ConcatRE>(subparts[0].value, std::make_shared<StarRE>(subparts[0].value));
    }
    std::shared_ptr<RE> ReParser::reduce_star(std::deque<RegexParser::Token> subparts) {
        return std::make_shared<StarRE>(subparts[0].value);
    }

	std::shared_ptr<RE> parseRE(const std::string& input) {
        std::stringstream ss(input); 
        RegexLexer lexer(ss);
        ReParser parser(lexer);
        auto res = parser.parse();
        return res;
    }


}
