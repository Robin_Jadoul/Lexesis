/*
Lexesis - A language agnostic lexical analyser generator
Copyright © 2016-2017 Thomas Avé, Robin Jadoul

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "Lexesis/driver.h"
#include "Lexesis/inputparser.h"
#include "Lexesis/automata.h"
#include "Lexesis/reParser.h"

#include <iostream>
#include <fstream>

namespace {
    /**
     * Filter only valid identifier chars: alphanumeric, and not starting with a digit
     */
    std::string clean(std::string in) {
        std::string s;
        for (char c : in) {
            if ((s.length() && std::isalnum(c)) || std::isalpha(c) || c == '_')
                s += c;
        }
        return s;
    }
    
    std::vector<lxs::ENFA> linesToEnfa(std::vector<std::pair<int, std::pair<std::string,std::string> > > &input) {
        std::vector<lxs::ENFA> result;
        for(unsigned int i=0;i<input.size();i++) {
                std::shared_ptr<lxs::RE> re = lxs::parseRE(input[i].second.second);
                lxs::ENFA enfa;
                re->toENFA(enfa,0);
                enfa.numStates++;
                enfa.starting = 0;
                enfa.priority[(lxs::State) *enfa.accepting.begin()] = (lxs::Priority) i; 
                enfa.acceptingToken[(lxs::State) *enfa.accepting.begin()] = input[i].second.first;
                result.push_back(enfa);
        }

        return result;
    }
}

namespace lxs {
    
    DriverException::DriverException(std::string what): m_what(what) {} 
    const char* DriverException::what() const throw() {
        return m_what.c_str();
    }
    
    Driver::Driver(std::unique_ptr<BackendManager> backends, std::istream& inputfile, std::string outputdir, std::string language, std::string lexername) :
                m_backends(std::move(backends)),
                m_inputfile(inputfile),
                m_outputdir(outputdir),
                m_language(language),
                m_lexername(clean(lexername))
    {}

    Driver::~Driver()
    {}

    int Driver::run() {
        if (!m_lexername.length()) throw DriverException("no valid lexer name possible");
        Backend* back = m_backends->findBackendForLang(m_language);
        if (!back) throw DriverException("Could not find a valid backend for language " + m_language );
        
        auto input = input::InputParser::parseInput(m_inputfile);
        auto enfas = linesToEnfa(input);
        auto enfa = merge(enfas);
        auto dfa = minimize(mssc(enfa));

        back->generateLexer([this](std::string filename) -> std::unique_ptr<std::ostream> {
                    return std::unique_ptr<std::ostream>(new std::ofstream(m_outputdir + "/" + filename));
                },
                m_lexername, dfa);

        return 0;
    }
}
