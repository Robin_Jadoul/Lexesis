/*
Lexesis - A language agnostic lexical analyser generator
Copyright © 2016-2017 Thomas Avé, Robin Jadoul

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "Lexesis/re.h"
#include "Lexesis/RegexLexer.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <stack>


namespace lxs {
    std::string EmptyRE::toRe()
    {
        return "∅";
    }

    State EmptyRE::toENFA(ENFA& enfa, State attach)
    {
        enfa.numStates = attach + 1;
        enfa.accepting.clear();
        enfa.accepting.insert(attach + 1);
        return attach + 1;
    }

    std::string EpsilonRE::toRe()
    {
        return "ε";
    }

    State EpsilonRE::toENFA(ENFA& enfa, State attach)
    {
        enfa.numStates = std::max(attach + 1, enfa.numStates);
        enfa.accepting.clear();
        enfa.accepting.insert(attach + 1);
        enfa.epsilonTransitions[attach].insert(attach + 1);
        return attach + 1;
    }

    std::string SingleRE::toRe()
    {
        return std::string(1, c);
    }

    State SingleRE::toENFA(ENFA& enfa, State attach)
    {
        enfa.numStates = std::max(attach + 1, enfa.numStates);
        enfa.accepting.clear();
        enfa.accepting.insert(attach + 1);
        enfa.delta[attach][c].insert(attach + 1);
        return attach + 1;
    }

    std::string MultiRE::toRe()
    {
        return "[" + std::string(chars.begin(), chars.end()) + "]";
    }

    State MultiRE::toENFA(ENFA& enfa, State attach)
    {
        enfa.numStates = std::max(attach + 1, enfa.numStates);
        enfa.accepting.clear();
        enfa.accepting.insert(attach + 1);
        for (char c : chars) {
            enfa.delta[attach][c].insert(attach + 1);
        }
        return attach + 1;
    }

    std::string ConcatRE::toRe()
    {
        return e->toRe() + f->toRe();
    }

    State ConcatRE::toENFA(ENFA& enfa, State attach)
    {
        State a = e->toENFA(enfa, attach);
        enfa.epsilonTransitions[a].insert(a + 1);
        return f->toENFA(enfa, a + 1);
    }

    std::string StarRE::toRe()
    {
        return "(" + e->toRe() + ")*";
    }

    State StarRE::toENFA(ENFA& enfa, State attach)
    {
        State a = e->toENFA(enfa, attach + 1);
        enfa.numStates = std::max(a + 1, enfa.numStates);
        enfa.accepting.clear();
        enfa.accepting.insert(a + 1);
        enfa.epsilonTransitions[attach].insert(attach + 1);
        enfa.epsilonTransitions[attach].insert(a + 1);
        enfa.epsilonTransitions[a].insert(attach + 1);
        enfa.epsilonTransitions[a].insert(a + 1);
        return a + 1;
    }

    std::string PlusRE::toRe()
    {
        return "(" + e->toRe() + "|" + f->toRe() + ")";
    }

    State PlusRE::toENFA(ENFA& enfa, State attach)
    {
        State a = e->toENFA(enfa, attach + 1);
        State b = f->toENFA(enfa, a + 1);
        enfa.numStates = std::max(enfa.numStates, b + 1);
        enfa.epsilonTransitions[attach].insert(attach + 1);
        enfa.epsilonTransitions[attach].insert(a + 1);
        enfa.epsilonTransitions[a].insert(b + 1);
        enfa.epsilonTransitions[b].insert(b + 1);
        enfa.accepting.clear();
        enfa.accepting.insert(b + 1);
        return b + 1;
    }

}
