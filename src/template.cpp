/*
Lexesis - A language agnostic lexical analyser generator
Copyright © 2016-2017 Thomas Avé, Robin Jadoul

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "Lexesis/template.h"
#include "mstch/mstch.hpp"

#include <fstream>  
#include <string>
#include <sstream> 

namespace templ {

    TemplateContext make_string(std::string _string) {
        return mstch::node(_string);
    }
    
    TemplateContext make_map(std::map<const std::string, TemplateContext> _map) {
        return mstch::map(_map);
    }
    
    TemplateContext make_array(std::vector<TemplateContext> _array) {
       return mstch::array(_array);
    }


    Template::Template(std::string filename) : m_filename(filename)
    {}

    Template::~Template()
    {}

    void Template::render(std::ostream& out, TemplateContext& context) {
		mstch::config::escape = [](const std::string& str) -> std::string {
			return str;
		};

        std::ifstream file;
        file.open (m_filename);
        std::stringstream tmplt;
        tmplt << file.rdbuf();
        file.close();
        out << mstch::render(tmplt.str(),context);
    }
}
