/*
Lexesis - A language agnostic lexical analyser generator
Copyright © 2016-2017 Thomas Avé, Robin Jadoul

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "Lexesis/automata.h"
#include "Lexesis/driver.h"
#include "Lexesis/reParser.h"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

int main(int argc, char** argv) {
    assert(argc == 3);

    std::vector<std::shared_ptr<lxs::RE> > regexes;
    std::string testname = argv[1];
    std::string outputdir = argv[2];

    std::string line;
    std::ofstream reOut(outputdir + "/" + testname + ".re");
    while (getline(std::cin, line)) {
        std::shared_ptr<lxs::RE> re = lxs::parseRE(line);
        reOut << re->toRe() << std::endl;
        regexes.push_back(re);
    }
    reOut.close();

    std::vector<lxs::ENFA> enfas;
    for (std::size_t i = 0; i < regexes.size(); i++) {
        lxs::ENFA enfa;
        regexes[i]->toENFA(enfa, 0);
        enfa.numStates++;
        enfa.starting = 0;
        enfa.priority[*enfa.accepting.begin()] = i;
        enfa.acceptingToken[*enfa.accepting.begin()] = std::to_string(i);
        enfas.push_back(enfa);
    }

    lxs::ENFA merged = lxs::merge(enfas);
    
    std::ofstream enfaOut(outputdir + "/" + testname + ".enfa");
    enfaOut << lxs::toDot(merged) << std::endl;
    enfaOut.close();

    lxs::DFA dfa = lxs::mssc(merged);
    std::ofstream msscOut(outputdir + "/" + testname + ".mssc");
    msscOut << lxs::toDot(dfa) << std::endl;
    msscOut.close();

    lxs::DFA min = lxs::minimize(dfa);
    std::ofstream minOut(outputdir + "/" + testname + ".min");
    minOut << lxs::toDot(min) << std::endl;
    minOut.close();

    return 0;
}
