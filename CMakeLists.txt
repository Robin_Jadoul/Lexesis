# Lexesis - A language agnostic lexical analyser generator
# Copyright © 2016-2017 Thomas Avé, Robin Jadoul
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
# OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

cmake_minimum_required(VERSION 3.2.2)
project(Lexesis VERSION 1.0)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED 11)


include(ExternalProject)
find_package(mstch QUIET)

if (NOT mstch_FOUND)
    ExternalProject_Add(ext-mstch
        GIT_REPOSITORY https://github.com/no1msd/mstch
        GIT_TAG 1.0.2
        #The following line is a dirty hack
        #to prevent cmake from needlessly clone any submodule
        #inspect `ext-mstch-prefix/tmp/ext-mstch-gitclone.cmake` for more info
        GIT_SUBMODULES "NONE\ COMMAND\ true\ ERROR_QUIET"
        CMAKE_ARGS -DWITH_UNIT_TESTS=NO
        INSTALL_COMMAND ""
        )

    add_library(mstch::mstch IMPORTED STATIC GLOBAL)
    add_dependencies(mstch::mstch ext-mstch)

    ExternalProject_Get_Property(ext-mstch source_dir binary_dir)
    set_target_properties(mstch::mstch PROPERTIES "IMPORTED_LOCATION" "${binary_dir}/src/${CMAKE_STATIC_LIBRARY_PREFIX}mstch${CMAKE_STATIC_LIBRARY_SUFFIX}")
    include_directories(${source_dir}/include)
endif()

ExternalProject_Add(ext-optparse
    GIT_REPOSITORY https://github.com/myint/optparse
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
    INSTALL_COMMAND ""
    )

ExternalProject_Get_Property(ext-optparse source_dir)
include_directories(${source_dir})
##########################################

find_package(Doxygen)
if(DOXYGEN_FOUND)
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)
    add_custom_target(doc ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR} COMMENT "Generating API documentation" VERBATIM)
endif(DOXYGEN_FOUND)

include_directories(include)
add_subdirectory(src)
add_dependencies(Lexesis ext-optparse)

install(DIRECTORY templates
    DESTINATION share/Lexesis
    )

install(DIRECTORY include/Lexesis
    DESTINATION include)

add_subdirectory(examples)

install(EXPORT LexesisTargets DESTINATION lib/cmake/Lexesis)
install(FILES LexesisConfig.cmake DESTINATION lib/cmake/Lexesis)
install(DIRECTORY man/man1 man/man5 DESTINATION man)
