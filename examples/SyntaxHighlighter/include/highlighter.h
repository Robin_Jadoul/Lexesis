/*
 * Lexesis - A language agnostic lexical analyser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.#pragma once
 */

#ifndef HIGHLIGHTER_H
#define HIGHLIGHTER_H

#include "XMLLexer.h"

#include <string>
#include <vector>
#include <map>
#include <iostream>

class Highlighter {
    public:
        /**
         * Create a new Highlighter with the content of the xml file
         */

        Highlighter(std::istream &file);
        virtual ~Highlighter();
        
        /**
         * Do the actual highlighting and write it to the provided ostream
         * This is a pure virtual function and has to be implemented when creating a new kind of highlighter
         */

        virtual void highlight(std::ostream &os)=0;

    protected:
       
        /**
        * Do the actual work with the lexer
        */ 

        void process();
        
        /**
         * Local Tokentype used to identify what part of the xml structure is inside the token
         * This is necessary because we use two different lexers with different TokenTypes
         */

        enum TokenType {
            COMMENT,
            TAG,
            CONTENT,
            ELEMENT,
            ATTRIBUTE,
            ATTRIBURE_CONTENT,
            WHITESPACE,
            BRACKET,
            nonmatching
        };
        
        /**
         * Represent all the different colors
         */

        enum Color { Red, Green, Blue, Orange, Yellow, Cyan, Grey, Black, White, Magenta, Pink, Brown, Indigo, Violet, Undefined}; // All the colors, not all of them are used, but it's easy to change now
    
        /**
         * A structure used to hold the tokens with their content, TokenType and color
         */

        struct Token {
            std::string content = "";
            Color color;
            TokenType type;
        };

        // Used to determine the color connected to the TokenType
        std::map<TokenType, Color> colormap;
        // Hold the tokens
        std::vector<Token> m_tokens;
        // Hold the first lexer
        XMLLexer *m_lexer;
};

class ConsoleHighlighter: public Highlighter {
    
    /**
     * A SyntaxHighlighter specifically for console output
     * Look at the base class for more information
     */

    public:
        ConsoleHighlighter(std::istream &file);
        void highlight(std::ostream &os);

};

#endif // HIGHLIGHTER_H
