/*
 * Lexesis - A language agnostic lexical analyser generator
 * Copyright © 2016-2017 Thomas Avé, Robin Jadoul
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "Lexer.h"
#include <iostream>

int main() {
    Lexer lex(std::cin);
    try {
        while (true) {
            Lexer::Token tok = lex.nextToken();
            switch (tok.type) {
                case Lexer::KEYBOARD_CAP:
                    std::cout << "Leopard";
                    break;
                case Lexer::KEYBOARD:
                    std::cout << "leopard";
                    break;
                case Lexer::OTHER:
                    std::cout << tok.content;
                    break;
                case Lexer::nonmatching: case Lexer::ignore:
                    std::cout << "Impossible" << std::endl;
                    break; //These can never occur, just to satisfy the compiler...
            }
        }
    }
    catch (Lexer::NoMatch& err) {
        std::cout << "Impossible" << std::endl;
    }
    catch (Lexer::NoMoreTokens& err) {
    }
}
